k_moves = [
    (2, 1), (2, -1), (-2, 1), (-2, -1),
    (1, 2), (1, -2), (-1, 2), (-1, -2)
]


def menu():
    """
    Меню программы
    """
    print('1. Найти количество ходов, необходимое коню, чтобы '
          'добраться из одной клетки шахматной доски до другой',
          '2. Найти минимальное количество ходов, через которое '
          'могут встретиться два коня',
          '0. Выйти из программы', sep='\n')
    print()


def knight_move(start: tuple, finish: tuple) -> int:
    """
    Функция для вычисления количества ходов, необходимого коню, чтобы
    добраться из одной клетки шахматной доски до другой

    :param start: Координаты стартовой позиции коня
    :type start: tuple
    :param finish: Координаты конечной позиции коня
    :type finish: tuple
    :return: Количество ходов
    :rtype: int
    """
    visit = [[0 for _ in range(8)]
             for _ in range(8)]
    calculate_move_matrix(start, visit)
    return visit[finish[0]][finish[1]] - 1


def knights_collision(start: tuple, finish: tuple) -> int:
    """
    Функция для вычисления минимального количества ходов для
    встречи двух коней, расположенных на двух разных клетках доски

    :param start: Координаты позиции первого коня
    :type start: tuple
    :param finish: Координаты позиции второго коня
    :type finish: tuple
    :return: Минимальное количество ходов
    :rtype: int
    """
    first_visit = [[0 for _ in range(8)]
                   for _ in range(8)]
    second_visit = [[0 for _ in range(8)]
                    for _ in range(8)]
    calculate_move_matrix(start, first_visit)
    calculate_move_matrix(finish, second_visit)
    return min(first_visit[j][i] for i in range(8) for j in range(8) if first_visit[j][i] - second_visit[j][i] == 0) - 1


def calculate_move_matrix(start: tuple, visit: list[list[int]], step: int = 1) -> None:
    """
    Функция для заполнения матрицы ходами коня

    :param start: Координаты коня
    :type start: tuple
    :param visit: Матрица
    :type visit: list[list[int]]
    :param step: Шаг
    :return: None
    :rtype: NoneType
    """
    visit[start[0]][start[1]] = step
    for move in k_moves:
        new_pos = (start[0] + move[0], start[1] + move[1])
        if is_inside(new_pos) and (visit[new_pos[0]][new_pos[1]] > step + 1 or visit[new_pos[0]][new_pos[1]] == 0):
            calculate_move_matrix(new_pos, visit, step + 1)


def is_inside(pos: tuple) -> bool:
    """
    Функция для проверки нахождения в рамках шахматной доски

    :param pos: Координаты позиции коня
    :type pos: tuple
    :return: True or False
    :rtype: bool
    """
    return (pos[0] >= 0) and (pos[1] >= 0) and (pos[0] < 8) and (pos[1] < 8)


def main():
    """
    Основная функция
    """
    flag = 1
    while flag:
        print()
        menu()
        item = input('Введите пункт меню ')
        try:
            item = int(item)
        except ValueError:
            print('Нет такого пункта меню!')
        if item == 0:
            flag = 0
        if item == 1 or item == 2:
            try:
                start = tuple(map(int, input('Введите первые координаты: ').split()))
                finish = tuple(map(int, input('Введите вторые координаты: ').split()))
            except (ValueError, TypeError, IndexError):
                print('Введенные данные некорректны!')
            else:
                if ((start[0] >= 1) and (start[1] >= 1) and (start[0] < 9) and (start[1] < 9)) and \
                            ((finish[0] >= 1) and (finish[1] >= 1) and (finish[0] < 9) and (finish[1] < 9)):
                    start = (start[0] - 1, start[1] - 1)
                    finish = (finish[0] - 1, finish[1] - 1)
                    if item == 1:
                        print(f"Необходимо ходов, чтобы добраться до точки: {knight_move(start, finish)}")
                    elif item == 2:
                        print(f"Необходимо ходов, чтобы кони встретились: {knights_collision(start, finish)}")
                else:
                    print('Введенные данные некорректны!')
        else:
            print('Нет такого пункта меню!')


main()
