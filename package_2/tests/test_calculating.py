import pytest


from package_2.calculating import knight_move, knights_collision


@pytest.fixture(autouse=True)
def start_tests():
    print('Тест начат')


data_for_move = [((1, 2), (1, 2), 0),
                 ((2, 3), (5, 6), 2),
                 ((2, 2), (2, 3), 3),
                 ((1, 1), (3, 3), 4)]


@pytest.mark.parametrize("input_1, input_2, expected", data_for_move)
def test_move(input_1, input_2, expected):
    assert knight_move(input_1, input_2) == expected


def test_move_with_error():
    with pytest.raises(TypeError):
        knight_move((1.2, 3), (3.4, 2))


data_for_collision = [((1, 2), (5, 6), 2),
                      ((6, 7), (6, 7), 0),
                      ((3, 4), (7, 2), 1),
                      ((1, 1), (6, 6), 2)]


@pytest.mark.parametrize("input_1, input_2, expected", data_for_collision)
def test_collision(input_1, input_2, expected):
    assert knights_collision(input_1, input_2) == expected


def test_collision_with_error():
    with pytest.raises(IndexError):
        knights_collision((10000, 100000), (10000, 100))
