import unittest


from package_2.calculating import knight_move, knights_collision


class MyTestCase(unittest.TestCase):
    def test_move_1(self):
        self.assertEqual(knight_move((5, 7), (5, 7)), 0)

    def test_move_2(self):
        self.assertEqual(knight_move((2, 5), (3, 6)), 2)

    def test_move_3(self):
        self.assertEqual(knight_move((2, 2), (4, 4)), 4)

    def test_move_error(self):
        with self.assertRaises(TypeError):
            knight_move((3.5, 2), (1, 7))

    def test_collision_1(self):
        self.assertEqual(knights_collision((6, 7), (5, 6)), 1)

    def test_collision_2(self):
        self.assertEqual(knights_collision((2, 3), (6, 7)), 2)

    def test_collision_error(self):
        with self.assertRaises(IndexError):
            knights_collision((999, 100), (10000, 800))

    def test_collision_error_2(self):
        with self.assertRaises(TypeError):
            knights_collision((2, 'a'), (4, 6))
