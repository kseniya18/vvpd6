from calculating import knight_move
from calculating import knights_collision


def menu():
    """
    Меню программы
    """
    print('1. Найти количество ходов, необходимое коню, чтобы '
          'добраться из одной клетки шахматной доски до другой',
          '2. Найти минимальное количество ходов, через которое '
          'могут встретиться два коня',
          '0. Выйти из программы', sep='\n')
    print()


def main():
    """
    Основная функция
    """
    flag = 1
    while flag:
        print()
        menu()
        item = input('Введите пункт меню ')
        try:
            item = int(item)
        except ValueError:
            print('Нет такого пункта меню!')
        if item == 0:
            flag = 0
        if item == 1 or item == 2:
            try:
                start = tuple(map(int, input('Введите первые координаты: ').split()))
                finish = tuple(map(int, input('Введите вторые координаты: ').split()))
            except (ValueError, TypeError, IndexError):
                print('Введенные данные некорректны!')
            else:
                if ((start[0] >= 1) and (start[1] >= 1) and (start[0] < 9) and (start[1] < 9)) and \
                            ((finish[0] >= 1) and (finish[1] >= 1) and (finish[0] < 9) and (finish[1] < 9)):
                    start = (start[0] - 1, start[1] - 1)
                    finish = (finish[0] - 1, finish[1] - 1)
                    if item == 1:
                        print(f"Необходимо ходов, чтобы добраться до точки: {knight_move(start, finish)}")
                    elif item == 2:
                        print(f"Необходимо ходов, чтобы кони встретились: {knights_collision(start, finish)}")
                else:
                    print('Введенные данные некорректны!')
        else:
            print('Нет такого пункта меню!')


if __name__ == '__main__':
    main()
