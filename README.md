# Про шахматного коня

_В шахматах конь является весьма уникальной фигурой, а всё из-за схемы 
его хода. За один ход конь может преодолеть маршрут 
напоминающий букву «Г». Также математически доказано, что конь за 
некоторое количество ходов может из любой клетки попасть в любую другую._

![ввпд6.3](/uploads/73fe178789cebe3876559a75a33ced49/ввпд6.3.png)

## Программная реализация

#### Вспомогательные функции

Функция _calculate_move_matrix_ заполняет матрицу ходами коня, а функция _is_inside_ проверяет нахождение в рамках шахматной доски.

```python
k_moves = [
    (2, 1), (2, -1), (-2, 1), (-2, -1),
    (1, 2), (1, -2), (-1, 2), (-1, -2)
]


def calculate_move_matrix(start: tuple, visit: list[list[int]], step: int = 1) -> None:
    """
    Функция для заполнения матрицы ходами коня

    :param start: Координаты коня
    :type start: tuple
    :param visit: Матрица
    :type visit: list[list[int]]
    :param step: Шаг
    :return: None
    :rtype: NoneType
    """
    visit[start[0]][start[1]] = step
    for move in k_moves:
        new_pos = (start[0] + move[0], start[1] + move[1])
        if is_inside(new_pos) and (visit[new_pos[0]][new_pos[1]] > step + 1 or visit[new_pos[0]][new_pos[1]] == 0):
            calculate_move_matrix(new_pos, visit, step + 1)


def is_inside(pos: tuple) -> bool:
    """
    Функция для проверки нахождения в рамках шахматной доски

    :param pos: Координаты позиции коня
    :type pos: tuple
    :return: True or False
    :rtype: bool
    """
    return (pos[0] >= 0) and (pos[1] >= 0) and (pos[0] < 8) and (pos[1] < 8)
```

#### Основные функции

##### Первая функция

Функция _knight_move_ вычисляет количество ходов, необходимое коню, чтобы добраться из одной клетки шахматной доски до другой.

```python
def knight_move(start: tuple, finish: tuple) -> int:
    """
    Функция для вычисления количества ходов, необходимого коню, чтобы
    добраться из одной клетки шахматной доски до другой

    :param start: Координаты стартовой позиции коня
    :type start: tuple
    :param finish: Координаты конечной позиции коня
    :type finish: tuple
    :return: Количество ходов
    :rtype: int
    """
    visit = [[0 for _ in range(8)]
             for _ in range(8)]
    calculate_move_matrix(start, visit)
    return visit[finish[0]][finish[1]] - 1
```

Функция _knights_collision_ вычисляет минимальное количество ходов для встречи двух коней, расположенных на двух разных клетках доски.

```python
def knights_collision(start: tuple, finish: tuple) -> int:
    """
    Функция для вычисления минимального количества ходов для
    встречи двух коней, расположенных на двух разных клетках доски

    :param start: Координаты позиции первого коня
    :type start: tuple
    :param finish: Координаты позиции второго коня
    :type finish: tuple
    :return: Минимальное количество ходов
    :rtype: int
    """
    first_visit = [[0 for _ in range(8)]
                   for _ in range(8)]
    second_visit = [[0 for _ in range(8)]
                    for _ in range(8)]
    calculate_move_matrix(start, first_visit)
    calculate_move_matrix(finish, second_visit)
    return min(first_visit[j][i] for i in range(8) for j in range(8) if first_visit[j][i] - second_visit[j][i] == 0) - 1
```
